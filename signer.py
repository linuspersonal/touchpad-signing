import evdev
import svgwrite
import pygame
import time
import os

MEASUREMENT_INTERVAL = 20  # Interval in ms, eg. 20 -> 1000/20 = 50Hz
LINE_THICKNESS = 6
INCH_IN_MM = 25.4

WINDOW_BG = (255, 255, 255)
DRAWING_COLOR = (0, 0, 0)

SIGNATURE_DIR = "Signatures/"
SVG_DIR = "Svg/"
FILE_PREFIX = "sig"


def get_unique_filename(prename: str, surname: str, width: int, height: int, width_mm: int, height_mm: int,
                        devmod: str, position: str, sex: str, age: int, country: str, language: str, hand: str) -> str:
    """
    Generates the filename for a signature based on the user information and the device information.
    :param prename: The first name of the user.
    :param surname: The last name of the user.
    :param width: The width of the touchpad in pixels.
    :param height: The height of the touchpad in pixels.
    :param width_mm: The width of the touchpad in mm, needed to calculate xdpi.
    :param height_mm: The height of the touchpad in mm, needed to calculate ydpi.
    :param devmod: The device model identifier.
    :param position: The recording position. Always sitting, just for compatibility reasons.
    :param sex: The sex of the user.
    :param age: The age of the user.
    :param country: The country the user is from.
    :param language: The main language of the user.
    :param hand: The hand used for signing.
    :return: Returns the unique filename as a string, in the common signature file format of the project.
    """
    cur_time_millis = int(round(time.time() * 1000))

    # Calculate DPI for the filename
    width_dpi = int(round(width / (width_mm / INCH_IN_MM)))
    height_dpi = int(round(height / (height_mm / INCH_IN_MM)))

    file_count = 0
    result = "{0}{1}-{2}-{3}-{4}-true-{5}-{6}-{7}-{8}-{9}-{10}-{11}-{12}-{13}-{14}-{15}".format(
        FILE_PREFIX, str(file_count), prename, surname,
        str(cur_time_millis), str(width), str(height),
        str(width_dpi), str(height_dpi), devmod,
        position, sex, str(age), country, language, hand)
    while os.path.isfile(SIGNATURE_DIR + "/" + result + ".csv"):
        file_count += 1
        result = "{0}{1}-{2}-{3}-{4}-true-{5}-{6}-{7}-{8}-{9}-{10}-{11}-{12}-{13}-{14}-{15}".format(
            FILE_PREFIX, str(file_count), prename, surname,
            str(cur_time_millis), str(width), str(height),
            str(width_dpi), str(height_dpi), devmod,
            position, sex, str(age), country, language, hand)

    return result


def lines_to_point_list(sig_lines) -> [(int, int, int, int)]:
    """
    Make one array of all point lists with stroke numbers and timestamps.
    :param sig_lines: The lines array containing the touch points for each stroke.
    :return: One array of points, together with timestamp and stroke number.
    """
    raw_points = []
    stroke_number = -1
    for line in sig_lines:
        stroke_number = stroke_number + 1
        for point in line:
            # x, y, strokeNumber, timestamp in ms
            timestamp = int(round(point[2], 3) * 1000)
            raw_points.append((point[0], point[1], stroke_number, timestamp))

    return raw_points


def interpolate_points(points_to_interpolate: [(int, int, int, int)], interval: int) -> [(int, int, int, int)]:
    """
    Interpolates the signature points to a fixed interval of 50 Hz, making sure that the last point is always the last
    signature point and ensuring stroke number consistency.
    :param points_to_interpolate: The raw recordings.
    :param interval: The time interval to interpolate to.
    :return: The interpolated points.
    """
    start_timestamp = points_to_interpolate[0][3]
    interpolated_points = []

    for i, curPoint in enumerate(points_to_interpolate):
        if i >= len(points_to_interpolate) - 1:
            # We are at the last item, just add
            rel_timestamp = curPoint[3] - start_timestamp
            interpolated_points.append((curPoint[0], curPoint[1], curPoint[2], rel_timestamp))
        else:
            # Not the last item, continue
            next_point = points_to_interpolate[i + 1]

            if next_point[3] > curPoint[3]:
                # Not the same timestamp as next, continue. Otherwise, just ignore current point
                amount = next_point[3] - curPoint[3]
                if amount == 1:
                    # Exactly 1ms apart, add current point
                    rel_timestamp = curPoint[3] - start_timestamp
                    interpolated_points.append((curPoint[0], curPoint[1], curPoint[2], rel_timestamp))
                else:
                    if next_point[2] == curPoint[2]:
                        # Same stroke, interpolate
                        for j in range(amount):
                            # Add current point and its interpolations up to, but not including the next point
                            x_inter = int(round(curPoint[0] + j * (next_point[0] - curPoint[0]) / amount))
                            y_inter = int(round(curPoint[1] + j * (next_point[1] - curPoint[1]) / amount))
                            rel_timestamp = curPoint[3] - start_timestamp + j
                            interpolated_points.append((x_inter, y_inter, next_point[2], rel_timestamp))
                    else:
                        # Different stroke, add current point and then null values
                        rel_timestamp_start = curPoint[3] - start_timestamp
                        interpolated_points.append((curPoint[0], curPoint[1], curPoint[2], rel_timestamp_start))
                        for j in range(1, amount):
                            interpolated_points.append((-1, -1, -1, rel_timestamp_start + j))

    # Apply measurement interval
    result = []
    for i in range(0, len(interpolated_points), interval):
        result.append(interpolated_points[i])

    last = result[len(result)-1]
    if last[0] < 0:
        print("FIXING LAST POINT IN INTERPOLATION")
        end_point = interpolated_points[len(interpolated_points)-1]

        # Check in which case the error would be smaller
        if (end_point[3] - last[3]) < int(interval/2):
            # Replace the faulty last line
            result[len(result)-1] = (end_point[0], end_point[1], end_point[2], last[3])
        else:
            # Add the correct line as next line
            result.append((end_point[0], end_point[1], end_point[2], last[3]+interval))

    # Make stroke number consistent - yes I know, interpolation is ugly already
    stroke_nr = -1
    new_stroke = True
    for i in range(0, len(result)):
        p = result[i]

        if p[2] < 0:
            new_stroke = True
        elif p[2] >= 0 and new_stroke:
            new_stroke = False
            stroke_nr += 1

        if p[2] >= 0:
            result[i] = (p[0], p[1], stroke_nr, p[3])

    return result


def save_as_csv(points: [(int, int, int, int)], directory: str, file: str):
    """
    Saves the interpolated signature as CSV file to be used in the toolchain.
    :param points: The interpolated points.
    :param directory: The directory to save to.
    :param file: The filename of the signature.
    """
    file = open(directory + file + ".csv", "w")
    file.write("x,y,strokenumber,timestamp\n")
    for i in range(0, len(points)):
        current_point = points[i]
        x_string = str(current_point[0]) if current_point[0] >= 0 else "null"
        y_string = str(current_point[1]) if current_point[1] >= 0 else "null"
        stroke_string = str(current_point[2]) if current_point[2] >= 0 else "null"
        file.write(x_string + "," + y_string + "," + stroke_string + "," + str(current_point[3]) + "\n")

    file.close()


def save_as_svg(points: [(int, int, int, int)], directory: str, file: str, width: int, height: int):
    """
    Saves the signature as SVG file.
    :param points: The interpolated signature points.
    :param directory: The directory to save to.
    :param file: The filename to use.
    :param width: The width of the svg in pixels.
    :param height: The height of the svg in pixels.
    """
    dwg = svgwrite.Drawing(directory + file + ".svg", size=(str(width) + "px", str(height) + "px"))

    last_point_stroke = 0
    _d = []
    x = 0
    for i in range(0, len(points)):
        pt = points[i]

        if pt[2] > last_point_stroke:
            # Next stroke begins, add current one to drawing and reset
            x = " ".join(_d).strip()
            dwg.add(dwg.path(d=x, stroke="#000", fill="none", stroke_width=LINE_THICKNESS))
            _d = []
            x = 0

        if pt[2] >= last_point_stroke:
            # Add point to line
            last_point_stroke = pt[2]
            if not x:
                _d.append("M {},{}".format(pt[0], pt[1]))
                x = 1
            else:
                _d.append("L {},{}".format(pt[0], pt[1]))

    # Add the last stroke
    x = " ".join(_d).strip()
    dwg.add(dwg.path(d=x, stroke="#000", fill="none", stroke_width=LINE_THICKNESS))

    # Write
    dwg.save()


def save_signature(sig_lines, name: str, width: int, height: int):
    """
    Saves a signature to svg and csv file, and creates directories if they don't exist yet. Performs interpolation on
    the raw signature data.
    :param sig_lines: The signature lines array, as recorded.
    :param name: The filename for the signature.
    :param width: The width for the svg.
    :param height: The height for the svg.
    """
    if not os.path.exists(SIGNATURE_DIR):
        os.makedirs(SIGNATURE_DIR)

    if not os.path.exists(SVG_DIR):
        os.makedirs(SVG_DIR)

    raw_points = lines_to_point_list(sig_lines)

    interpolated_points = interpolate_points(raw_points, MEASUREMENT_INTERVAL)

    save_as_csv(interpolated_points, SIGNATURE_DIR, name)

    save_as_svg(interpolated_points, SVG_DIR, name, width, height)


first_name = input("First Name: ").replace("-", "")
last_name = input("Last Name: ").replace("-", "")

disclaimer = input("I have read, understood and agree to the disclaimer, "
                   "especially the publishing of my signatures (yes/no): ").casefold()
while not (disclaimer == "yes" or disclaimer == "no"):
    disclaimer = input("I have read, understood and agree to the disclaimer, "
                          "especially the publishing of my signatures (yes/no): ").casefold()

if disclaimer == "yes":
    print("You agreed to the disclaimer. Recording is possible.")
else:
    print("You do not agree to the disclaimer, so no signatures can be recorded. Exiting.")
    exit(0)

device_model = input("Device Model: ").replace(" ", "").replace("-", "")  # Acer Aspire VN7-571G-573Q
tp_width_mm = int(input("Width of the touchpad in mm: "))  # 106 mm - My Acer Laptop
tp_height_mm = int(input("Height of the touchpad in mm: "))  # 78 mm - My Acer Laptop
recording_position = "sitting"
sex = input("Sex (male/female): ")
age = int(input("Age: "))
country = input("Country: ")
language = input("Language: ")
hand = input("Signing hand (left/right): ")


# Get input Device
touch_device = None
device_list = [evdev.InputDevice(path) for path in evdev.list_devices()]
for device in device_list:
    if "touchpad" in device.name.casefold():
        touch_device = device

if touch_device is None:
    print("No touchpad was found. Exit.")
    exit()

print("Selected {} {} {}".format(touch_device.path, touch_device.name, touch_device.phys))

# Get Bounding box
print("")
print(
    "Move your finger around your trackpad to the extremes to the bounding box. STRG + C to commit the maximum values")
print("")

tp_width = 0
tp_height = 0

try:
    for event in touch_device.read_loop():
        if event.code == evdev.ecodes.ABS_MT_POSITION_X:
            if event.value > tp_width:
                tp_width = event.value
        if event.code == evdev.ecodes.ABS_MT_POSITION_Y:
            if event.value > tp_height:
                tp_height = event.value

        print("({:>4}:{:>4})".format(tp_width, tp_height), end="\r")

except KeyboardInterrupt:
    print("\nBounding box set at ({}, {})".format(tp_width, tp_height))

saved_signature_count = 0
continue_recording = True
while continue_recording:
    print("Signatures recorded so far: ", saved_signature_count)
    print()
    print("Capturing Drawing. All contact with the touchpad will be tracked.")
    print("A new window will open where you can see your signature while drawing.")
    print("When finished signing, don't touch the touchpad anymore and switch back to the console using: ")
    print("ALT + TAB (or a similar command on your system)")
    print("The mouse will not be available until you continue after drawing the signature.")
    input("Press Enter to start")

    # Set window position and open window the width and height of the touchpad
    os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (0, 0)
    pygame.init()
    screen = pygame.display.set_mode((tp_width, tp_height))
    pygame.mouse.set_visible(False)

    screen.fill(WINDOW_BG)
    pygame.display.update()

    prev_x, prev_y, curr_x, curr_y = -1, -1, -1, -1
    touch = False

    lines = []
    currLine = []
    setMarker = 0

    print("------------------")
    print("All touchpad interactions are being tracked")
    print("To continue, press CTRL + C")

    # Events thrown regardless of mouse movement - 270 movement-events per second, but apparently not constantly.
    # Far more with fast mouse movement (ca. 800)
    try:
        for event in touch_device.read_loop():
            # Lock mouse in place
            pygame.mouse.set_pos(tp_width / 2, tp_height / 2)

            if event.code == evdev.ecodes.ABS_MT_POSITION_X:
                setMarker += 1
                curr_x = event.value
            if event.code == evdev.ecodes.ABS_MT_POSITION_Y:
                setMarker += 1
                curr_y = event.value
            if event.code == evdev.ecodes.BTN_TOUCH:
                if touch:
                    lines.append(currLine)
                    currLine = []
                    touch = False
                    prev_x, prev_y, curr_x, curr_y = -1, -1, -1, -1
                    # Release event grab so ALT + TAB works when not currently drawing
                    pygame.event.set_grab(False)

                else:
                    touch = True
                    # Capture all events so mouse clicks don't minimize the window while drawing signature
                    pygame.event.set_grab(True)

            if touch and (curr_x != prev_x or curr_y != prev_y) and not setMarker % 2:

                if prev_x >= 0 and prev_y >= 0 and curr_x >= 0 and curr_y >= 0:
                    pygame.draw.lines(screen, DRAWING_COLOR, False, [(prev_x, prev_y), (curr_x, curr_y)], 3)
                    pygame.display.update()

                prev_x = curr_x
                prev_y = curr_y

                currLine.append((curr_x, curr_y, time.monotonic()))

    except KeyboardInterrupt:
        lines.append(currLine)
        pygame.quit()
        pass

    print()
    if len(lines) < 1:
        print("Signature is empty, nothing can be saved.")
    else:
        saveSignature = input("Save signature? (Y/N): ").casefold()
        while not (saveSignature == "y" or saveSignature == "n"):
            saveSignature = input("Save signature? (Y/N): ").casefold()

        if saveSignature == "n":
            print("Signature not saved")
        else:
            f = get_unique_filename(first_name, last_name, tp_width, tp_height, tp_width_mm, tp_height_mm, device_model,
                                    recording_position, sex, age, country, language, hand)
            save_signature(lines, f, tp_width, tp_height)

            saved_signature_count += 1
            print("Signature saved. Number of saved signatures: " , saved_signature_count)

    record_next = input("Record another signature? (Y/N/C): ").casefold()
    while not (record_next == "y" or record_next == "n" or record_next == "c"):
        record_next = input("Record another signature? (Y/N/C): ").casefold()

    if record_next == "n":
        continue_recording = False
    elif record_next == "c":
        print("Name change selected (for forgery mode)")
        first_name = input("First Name: ").replace("-", "")
        last_name = input("Last Name: ").replace("-", "")
        saved_signature_count = 0
        print("Resetting saved signature count to", saved_signature_count)

print("Recording session finished,", saved_signature_count, "have been saved.")
print("Answer questionnaire after second session:", "https://forms.gle/tLKwBPFe1Mcm65HFA")
