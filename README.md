This is the repository for our touchpad signing application. It is a python script that only works under linux, as windows does not allow direct access to the touchpad coordinate data output by the touchpad device.

The use of the program is self-explanatory, once you start it an interactive command line will take you through the process.

Please note that the user has to be added to the device input group with 'sudo usermod -a -G input yourusername' in order to access /dev/input without sudo. This is bad for security reasons - change it back after use if you are doing this on your productive system.
